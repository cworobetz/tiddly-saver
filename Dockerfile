
FROM golang:alpine AS build-env

WORKDIR $GOPATH/src/tiddly-saver

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

ENV GO111MODULE=on
RUN go build -o /go/bin/tiddly-saver

FROM scratch
COPY --from=build-env /go/bin/tiddly-saver /srv/tiddly-saver

WORKDIR /srv
ENTRYPOINT [ "./tiddly-saver" ]