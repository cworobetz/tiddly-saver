module gitlab.com/cworobetz/tiddly-saver

go 1.16

require (
	fyne.io/systray v1.10.0
	github.com/fsnotify/fsnotify v1.6.0
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
